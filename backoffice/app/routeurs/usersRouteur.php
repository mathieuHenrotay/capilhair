<?php
/*
    ./app/routeurs/usersRouteur.php
    Routeur des users
*/
include_once '../app/controleurs/usersControleur.php';
switch ($_GET['users']) {
  case 'deconnexion':
    // ROUTE DE DECONNEXION
    // PATTERN: users/deconnexion
    // CTRL:usersControleur
    // ACTION: disconnectAction

    \App\Controleurs\Users\disconnectAction();
    break;
}
