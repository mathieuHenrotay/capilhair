<?php
/*
    ./app/routeur.php
    Routeur principal
    Il décide quelle action de quel contrôleur il faut lancer
 */
      if(isset($_GET['users'])):
        include_once '../app/routeurs/usersRouteur.php';
      else:
        // ROUTE PAR DEFAUT
        // PATTERN: /
        // CTRL: usersControleur
        // ACTION: dashboard
        include_once '../app/controleurs/usersControleur.php';
        \App\Controleurs\Users\dashboardAction($connexion);
      endif;
