/*
./www/js/ajaxIndex.js
 script AJAX de la liste des projets
*/
$(function(){
  var offset=$('#moreProjects').closest('div').find('>div').length;
  $('#moreProjects').click(function(e){
    e.preventDefault();
    /*$.ajax({
        url: 'projets/ajax/index',
        data: {
          offset:offset
        },
        method: 'POST',
        success: function(reponsePHP){
          if(reponsePHP!=''){
            //je récupère le nombre d'éléments ajoutés
            //en coupant après chaque élément
            var tabs=reponsePHP.split('<!-- /.row -->');
             //la dernière chaine est vide
            nbreElem=tabs.length-1;
            //j'ajoute le contenu, je cache les x derniers et je les slideDown
            $("#moreProjects").before(reponsePHP).closest('div').find('>div:nth-last-of-type(-n+'+nbreElem+')').hide().slideDown(1500);
            //j'incrémente l'offset
            offset+=nbreElem;
          }
          else{
            alert('Il n\'y a pas d\'autres projets à afficher, désolé !');
            $("#moreProjects").remove();
          }

        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });*/
    $.ajax({
        url: 'projets/ajax/index',
        data: {
          offset:offset
        },
        method: 'POST',
        success: (reponsePHP)=>{
          if(reponsePHP!=''){
            //j'ajoute le contenu et je récupère le nombre d'éléments ajoutés grace au data-ajout qui vient du serveur
            // je cache les x derniers et je les slideDown
            var nbreElem=parseInt($(this).before(reponsePHP).siblings('div').last().attr('data-ajout'));
            $(this).closest('div').find('>div:nth-last-of-type(-n+'+nbreElem+')').hide().slideDown(1500);


            //j'incrémente l'offset
            offset+=nbreElem;
          }
          else{
            alert('Il n\'y a pas d\'autres projets à afficher, désolé !');
            $(this).remove();
          }

        },
        error: function(){
          window.alert("Problème durant la transaction...");
        }
    });
  });

});
