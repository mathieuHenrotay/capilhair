<?php
/*
    ./noyau/fonctions.php
    Fonctions personnalisées du framework
 */
namespace App\Noyau\Fonctions;

function slugify(string $str) {
  return trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($str)), '-');
}

function securify(string $str){
  return "`".str_replace("`","``",htmlentities($str))."`";
}

function datify(STRING $date, STRING $format){
  return date_format(date_create($date),$format);
}

function truncate(STRING $texte, INT $caract){
  return substr($texte, 0, strrpos(substr($texte, 0, $caract), ' ')).' ...';
}

function params_merge(ARRAY $params){
  $params_defaut = [
    'orderBy' => 'id',
    'orderSens' => 'asc',
    'limit' => null,
      'offset' => null
  ];
  $params=array_merge($params_defaut, $params);
  return $params;
}
