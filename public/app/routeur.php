<?php
/*
    ./app/routeur.php
    Routeur principal
    Il décide quelle action de quel contrôleur il faut lancer
 */
if(isset($_GET['users'])):
   include_once '../app/routeurs/usersRouteur.php';
elseif(isset($_GET['pages'])):
  include_once '../app/routeurs/pagesRouteur.php';
elseif(isset($_GET['projets'])):
  include_once '../app/routeurs/projetsRouteur.php';
elseif(isset($_GET['abonnes'])):
  include_once '../app/routeurs/abonnesRouteur.php';
elseif(isset($_GET['tags'])):
  include_once '../app/routeurs/tagsRouteur.php';
elseif(isset($_GET['creatifs'])):
  include_once '../app/routeurs/creatifsRouteur.php';
else:

  // ROUTE PAR DEFAUT(details page 1)
  // PATTERN: /
  // CTRL:pagesControleur
  // ACTION: showAction

  include_once '../app/controleurs/pagesControleur.php';
  \App\Controleurs\Pages\showAction($connexion);

endif;
