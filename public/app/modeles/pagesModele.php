<?php
/*
    ./app/modeles/pagesModele.php
    Modèle des pages
*/
namespace App\Modeles\Page;
function findOneById(\PDO $connexion, INT $id=1){

    $sql="SELECT *
          FROM pages
          WHERE id= :id;";
    $rs=$connexion->prepare($sql);
    $rs->bindValue(':id',$id,\PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
}

function findAll(\PDO $connexion, ARRAY $params=[]){
    $params=\App\Noyau\Fonctions\params_merge($params);
    $orderSens=htmlentities($params['orderSens']); //securify ne fonctionne pas
    $orderBy=\App\Noyau\Fonctions\securify($params['orderBy']);
    $sql="SELECT *
          FROM pages
          ORDER BY $orderBy $orderSens ";

    ($params['limit'])?$sql.="LIMIT :limit ":'';
    ($params['offset'])?$sql.="OFFSET :offset ":'';

    $sql.=";";

    $rs=$connexion->prepare($sql);
    ($params['limit'])?$rs->bindValue(':limit', $params['limit'],\PDO::PARAM_INT):'';
    ($params['offset'])?$rs->bindValue(':offset', $params['offset'],\PDO::PARAM_INT):'';

    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
