<?php
/*
    ./app/modeles/projetsModele.php
    Modèle des projets
*/
namespace App\Modeles\Projet;
function findAll(\PDO $connexion, ARRAY $params=[]){
    $params=\App\Noyau\Fonctions\params_merge($params);
    $orderSens=htmlentities($params['orderSens']); //securify ne fonctionne pas
    $orderBy=\App\Noyau\Fonctions\securify($params['orderBy']);
    $sql="SELECT *,
          projets.id AS projetId,
          creatifs.id AS creatifId,
          projets.slug AS projetSlug,
          creatifs.slug AS  creatifSlug,
          projets.image AS projetImage,
          creatifs.image AS creatifImage
          FROM projets
          JOIN creatifs ON creatifs.id=creatif
          ORDER BY $orderBy $orderSens ";

    ($params['limit'])?$sql.="LIMIT :limit ":'';
    ($params['offset'])?$sql.="OFFSET :offset ":'';

    $sql.=";";

    $rs=$connexion->prepare($sql);
    ($params['limit'])?$rs->bindValue(':limit', $params['limit'],\PDO::PARAM_INT):'';
    ($params['offset'])?$rs->bindValue(':offset', $params['offset'],\PDO::PARAM_INT):'';

    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findAllByTagId(\PDO $connexion, ARRAY $params=[]){
    $params=\App\Noyau\Fonctions\params_merge($params);
    $orderSens=htmlentities($params['orderSens']); //securify ne fonctionne pas
    $orderBy=\App\Noyau\Fonctions\securify($params['orderBy']);
    $sql="SELECT *,
          projets.id AS projetId,
          creatifs.id AS creatifId,
          projets.slug AS projetSlug,
          creatifs.slug AS  creatifSlug,
          projets.image AS projetImage,
          creatifs.image AS creatifImage
          FROM projets
          JOIN creatifs ON creatifs.id=creatif
          WHERE projets.id IN (SELECT projet
                        FROM projets_has_tags
                        WHERE tag=:id)
          ORDER BY $orderBy $orderSens ";

    ($params['limit'])?$sql.="LIMIT :limit ":'';
    ($params['offset'])?$sql.="OFFSET :offset ":'';

    $sql.=";";

    $rs=$connexion->prepare($sql);
    ($params['limit'])?$rs->bindValue(':limit', $params['limit'],\PDO::PARAM_INT):'';
    ($params['offset'])?$rs->bindValue(':offset', $params['offset'],\PDO::PARAM_INT):'';
    $rs->bindValue(':id', $params['id'],\PDO::PARAM_INT);

    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findAllByCreatifId(\PDO $connexion, ARRAY $params=[]){
    $params=\App\Noyau\Fonctions\params_merge($params);
    $orderSens=htmlentities($params['orderSens']); //securify ne fonctionne pas
    $orderBy=\App\Noyau\Fonctions\securify($params['orderBy']);
    $sql="SELECT *,
          projets.id AS projetId,
          creatifs.id AS creatifId,
          projets.slug AS projetSlug,
          creatifs.slug AS  creatifSlug,
          projets.image AS projetImage,
          creatifs.image AS creatifImage
          FROM projets
          JOIN creatifs ON creatifs.id=creatif
          WHERE creatif=:id
          ORDER BY $orderBy $orderSens ";

    ($params['limit'])?$sql.="LIMIT :limit ":'';
    ($params['offset'])?$sql.="OFFSET :offset ":'';

    $sql.=";";

    $rs=$connexion->prepare($sql);
    ($params['limit'])?$rs->bindValue(':limit', $params['limit'],\PDO::PARAM_INT):'';
    ($params['offset'])?$rs->bindValue(':offset', $params['offset'],\PDO::PARAM_INT):'';
    $rs->bindValue(':id', $params['id'],\PDO::PARAM_INT);

    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findOneById(\PDO $connexion, INT $id=null){
  if($id):
    $sql="SELECT *,
          projets.id AS projetId,
          creatifs.id AS creatifId,
          projets.slug AS projetSlug,
          creatifs.slug AS  creatifSlug,
          projets.image AS projetImage,
          creatifs.image AS creatifImage
          FROM projets
          JOIN creatifs ON creatifs.id=creatif
          WHERE projets.id= :id;";
    $rs=$connexion->prepare($sql);
    $rs->bindValue(':id',$id,\PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  else:
    return 0;
  endif;
}
