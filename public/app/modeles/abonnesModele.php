<?php
/*
    ./app/modeles/abonnesModele.php
    Modèle des abonnes
*/
namespace App\Modeles\Abonne;
function insertOne(\PDO $connexion, STRING $mail=null){
  if($mail):
    $sql="INSERT INTO abonnes
          SET mail=:mail;";

    $rs=$connexion->prepare($sql);
    $rs->bindValue(':mail', $mail, \PDO::PARAM_STR);
    $rs->execute();
    return $connexion->lastInsertId();
  else:
    return 0;
  endif;
}
