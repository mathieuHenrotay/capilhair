<?php
/*
    ./app/vues/tags/index.php
    variables disponibles:
    $tags ARRAY DE ARRAY : id, nom, slug
*/
?>
<?php foreach ($tags as $tag): ?>
  <li>
    <a href="tags/<?php echo $tag['id']; ?>/<?php echo $tag['slug']; ?>"><?php echo $tag['nom']; ?></a>
  </li>
<?php endforeach; ?>
