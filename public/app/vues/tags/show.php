<?php
/*
    ./app/vues/tags/show.php
    variables disponibles:
    $tagRecherche ARRAY ASSOC: (id, nom, slug)
    $projets ARRAY DE ARRAY (projetId, titre, texte, dateCreation, projetImage, creatif, projetSlug,creatifId, pseudo, bio, creatifSlug, creatifImage )
    $tags ARRAY DE ARRAY (id, nom, slug)
*/
?>
<h1 class="mt-4">Résultats de votre recherche : <small><?php echo $tag['nom']; ?></small></h1>

<hr>
<?php if ($projets): ?>
  <?php include '../app/vues/projets/partials/liste.php'; ?>
<?php else: ?>
    <p>Désolé aucun projet ne correspond à votre recherche</p>
    <div>
      <a class="btn btn-primary" href="" style="margin-bottom:1rem;">Vers l'accueil</a>
    </div>
<?php endif; ?>
