<?php
/*
    ./app/vues/projets/show.php
    variables disponibles:
    $projet ARRAY (projetId, titre, texte, dateCreation, projetImage, creatif, projetSlug,creatifId, pseudo, bio, creatifSlug, creatifImage )
    $tags ARRAY DE ARRAY (id, nom, slug)
*/
?>
<!-- Page Heading -->

<!-- Title -->
<h1 class="mt-4"><?php echo $projet['titre']; ?></h1>
<p class="lead">
  par
  <a href="creatifs/<?php echo $projet['creatif']; ?>/<?php echo $projet['creatifSlug']; ?>"><?php echo $projet['pseudo']; ?></a> le <?php echo \App\Noyau\Fonctions\datify($projet['dateCreation'], 'd-m-Y'); ?>
</p>

<hr>




<!-- Project One -->
<div class="row">
  <div class="col-md-6">
    <a href="#">
      <img class="img-fluid rounded mb-3 mb-md-0" src="images/<?php echo $projet['projetImage']; ?>" alt="">
    </a>
  </div>
  <div class="col-md-6">
    <?php echo $projet['texte']; ?>
    <hr/>
    <ul class="list-inline tags">
      <?php foreach ($tags as $tag): ?>
        <li><a href="#" class="btn btn-default btn-xs"><?php echo $tag['nom']; ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<!-- /.row -->
<hr>
