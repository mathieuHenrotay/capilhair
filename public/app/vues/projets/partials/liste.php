<?php
/*
    ./app/vues/projets/liste.php
    variables disponibles:
    $projets ARRAY DE ARRAY (projetId, titre, texte, dateCreation, projetImage, creatif, projetSlug,creatifId, pseudo, bio, creatifSlug, creatifImage )
    $tags ARRAY DE ARRAY (id, nom, slug)
*/
?>
<?php foreach ($projets as $projet): ?>
  <!--Le data-ajout est uniquement utile pour mon ajax-->
  <div class="row" data-ajout="<?php echo count($projets); ?>">
    <div class="col-md-4">
      <a href="projets/<?php echo $projet['projetId']; ?>/<?php echo $projet['projetSlug']; ?>">
        <img class="img-fluid rounded mb-3 mb-md-0" src="images/<?php echo $projet['projetImage']; ?>" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3><?php echo $projet['titre']; ?></h3>
      <p class="lead">
        par
        <a href="creatifs/<?php echo $projet['creatif']; ?>/<?php echo $projet['creatifSlug']; ?>"><?php echo $projet['pseudo']; ?></a> le <?php echo \App\Noyau\Fonctions\datify($projet['dateCreation'], 'd-m-Y'); ?>
      </p>
      <p><?php echo \App\Noyau\Fonctions\truncate($projet['texte'],100); ?></p>
      <a class="btn btn-primary" href="projets/<?php echo $projet['projetId']; ?>/<?php echo $projet['projetSlug']; ?>">View Project</a>
      <hr/>
      <ul class="list-inline tags">
        <?php
          include_once '../app/modeles/tagsModele.php';
          $tags=\App\Modeles\Tag\findAllByProjetId($connexion, [
            'orderBy'=>'nom',
            'orderSens'=>'ASC',
            'projetId'=>$projet['projetId']
          ]);
         ?>
        <?php foreach ($tags as $tag): ?>
          <li><a href="tags/<?php echo $tag['id']; ?>/<?php echo $tag['slug']; ?>" class="btn btn-default btn-xs"><?php echo $tag['nom']; ?></a></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <!-- /.row -->

  <hr>
<?php endforeach; ?>
