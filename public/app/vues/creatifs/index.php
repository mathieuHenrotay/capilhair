<?php
/*
    ./app/vues/creatifs/index.php
    variables disponibles:
    $creatifs ARRAY DE ARRAY (id pseudo bio slug image)
*/
?>
<?php foreach ($creatifs as $creatif): ?>
  <div class="row">
    <div class="col-md-4">
      <a href="creatifs/<?php echo $creatif['id']; ?>/<?php echo $creatif['slug']; ?>">
        <img class="img-fluid rounded mb-3 mb-md-0" src="images/<?php echo $creatif['image'] ?>" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3><?php echo $creatif['pseudo']; ?></h3>
      <p><?php echo \App\Noyau\Fonctions\truncate($creatif['bio'],100); ?></p>
      <a class="btn btn-primary" href="creatifs/<?php echo $creatif['id']; ?>/<?php echo $creatif['slug']; ?>">Voir détails</a>
      <hr/>
    </div>
  </div>
  <hr/>
<?php endforeach; ?>
