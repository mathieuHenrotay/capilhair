<?php
/*
    ./app/vues/creatifs/show.php
    variables disponibles:
    $creatif ARRAY ASSOC (id pseudo bio slug image)
    $projets ARRAY DE ARRAY (projetId, titre, texte, dateCreation, projetImage, creatif, projetSlug,creatifId, pseudo, bio, creatifSlug, creatifImage )
    $tags ARRAY DE ARRAY (id, nom, slug)

*/
?>
<!-- Title -->
<h1 class="mt-4"><?php echo $creatif['pseudo']; ?></h1>

<hr>

<p class="lead"><?php echo $creatif['bio']; ?></p>

<hr>

<?php include '../app/vues/projets/partials/liste.php'; ?>
