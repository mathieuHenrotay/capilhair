<?php
/*
    ./app/vues/templates/partials/newsletter.php
*/
?>
<div class="card my-4">
  <h5 class="card-header">Newsletter</h5>
  <div class="card-body">
    <div class="input-group">
      <form action="newsletter" method="post">
        <input type="text" placeholder="Votre mail" name="mail">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="submit">Go!</button>
        </span>
      </form>
    </div>
  </div>
</div>
