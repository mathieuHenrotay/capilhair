<?php
/*
    ./app/vues/templates/partials/nav.php
*/
?>
<div class="container">
  <a class="navbar-brand" href="#">CREA'TIFs - Design capill'Hair</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav ml-auto">
      <?php
        // ROUTE DE MENU
        // PATTERN: /
        // CTRL:pagesControleur
        // ACTION: indexAction

        include_once '../app/controleurs/pagesControleur.php';
        \App\Controleurs\Pages\indexAction($connexion, [
          'orderBy'=>'tri',
          'orderSens'=>'ASC'
        ]);
       ?>
    </ul>
  </div>
</div>
