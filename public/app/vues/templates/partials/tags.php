<?php
/*
    ./app/vues/templates/partials/tags.php
*/
?>
<div class="card my-4">
  <h5 class="card-header">Tags</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <ul class="list-unstyled mb-0">
          <?php
            // ROUTE DE LA LISTE DES TAGS
            // PATTERN: /
            // CTRL:tagsControleur
            // ACTION: indexAction

            include_once '../app/controleurs/tagsControleur.php';
            \App\Controleurs\Tags\indexAction($connexion, [
              'orderBy'=>'nom',
              'orderSens'=>'asc'
            ]);
           ?>
        </ul>
      </div>

    </div>
  </div>
</div>
