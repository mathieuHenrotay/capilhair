<?php
/*
    ./app/vues/templates/defaut.php
    Template par défaut
 */
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <?php include '../app/vues/templates/partials/head.php'; ?>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <?php include '../app/vues/templates/partials/nav.php'; ?>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

            <?php echo $content1; ?>





        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <?php include '../app/vues/templates/partials/newsletter.php'; ?>

          <!-- Categories Widget -->
          <?php include '../app/vues/templates/partials/tags.php'; ?>


        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <?php include '../app/vues/templates/partials/footer.php'; ?>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <?php include '../app/vues/templates/partials/scripts.php'; ?>

  </body>

</html>
