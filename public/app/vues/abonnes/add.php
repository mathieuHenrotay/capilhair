<?php
/*
    ./app/vues/abonnes/add.php
    variables disponibles: $mail string, $id INT
*/
?>
<?php
if (intval($id)!=0):
  $titre='C\'est un franc succès !';
  $message='Votre adresse mail '.$mail.' a bien été rajoutée à notre base de données, merci !';
  $imageSrc="succes.jpg";
else:
  $titre='AAARGH.. Quelque chose s\'est mal passé !';
  $message='Erreur lors de l\'ajout à la base de données, réessayez plus tard.';
  $imageSrc="failure.png";
endif; ?>
<h1 class="mt-4"><?php echo $titre; ?></h1>

<hr>

<p class="lead"><?php echo $message; ?></p>
<img src="images/<?php echo $imageSrc ?>" alt="" style="max-width:100%;">
<hr>
