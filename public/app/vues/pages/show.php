<?php
/*
    ./app/vues/pages/show.php
    variables disponibles:
      $page ARRAY ASSOC (id, titre, slug, texte, tri)
*/
?>
<!-- Page Heading -->

<!-- Title -->
<h1 class="mt-4"><?php echo $page['titre']; ?></h1>

<hr>


<!-- Post Content -->
<p class="lead"><?php echo $page['texte']; ?></p>




<hr>
<?php
  switch ($page['id']) {
    case 1:
      // ROUTE DE LA LISTE DES POSTS
      // PATTERN: /
      // CTRL:projetsControleur
      // ACTION: indexAction

      echo '<h2 class="mt-4">Derniers projets</h2><hr>';
      include_once '../app/controleurs/projetsControleur.php';
      \App\Controleurs\Projets\indexAction($connexion, [
        'orderBy'=>'dateCreation',
        'orderSens'=>'DESC',
        'limit'=>2
      ]);
      break;
    case 2:
      // ROUTE DE LA LISTE DES PROJETS
      // PATTERN: /
      // CTRL:projetsControleur
      // ACTION: indexAction
      
      include_once '../app/controleurs/projetsControleur.php';
      \App\Controleurs\Projets\indexAction($connexion, [
        'orderBy'=>'dateCreation',
        'orderSens'=>'DESC',
        'limit'=>10
      ]);
      break;
    case 3:
      // ROUTE DE LA LISTE DES CREATIFS
      // PATTERN: /
      // CTRL:creatifsControleur
      // ACTION: indexAction

      include_once '../app/controleurs/creatifsControleur.php';
      \App\Controleurs\Creatifs\indexAction($connexion, [
        'orderBy'=>'pseudo',
        'orderSens'=>'ASC'
      ]);
      break;
  }
?>
