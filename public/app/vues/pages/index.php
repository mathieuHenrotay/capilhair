<?php
/*
    ./app/vues/pages/index.php
    variables disponibles:
    $pages ARRAY DE ARRAY (id, titre, slug, texte, tri)
*/
?>
<!--FACON PLUS COMPLEXE AVEC GESTION DU HIGHLIGHT-->
  <!--Je suis obligé de passer par un for car j'ai besoin de l'index pour highlighter l'élément du menu actif-->
  <?php for($i=0;$i<count($pages);$i++):?>
    <!--S'il on est sur le detail d'une page(où $_GET['pages']='show'), je mets actif l'élément du menu en rapport(je récupère l'id présent en get et je fais -1 car les id commence à 1 et mon for commence à 0)-->
    <!--Si je suis sur la page d'accueil(ou il n'y a aucune variable get définie), je vérifie que je n'ai rien dans la variable globale $_GET et je mets actif le premier élément de menu(Accueil)-->
    <li class="nav-item <?php if (($_GET['pages']=='show'&&$i==$_GET['id']-1) || ($_GET==[]&&$i==0)): ?>
          active
      <?php endif; ?>">
      <a class="nav-link" href="pages/<?php echo $pages[$i]['id'];?>/<?php echo $pages[$i]['slug']; ?>"><?php echo $pages[$i]['titre']; ?></a>
    </li>
  <?php endfor; ?>

<!--FACON CLASSIQUE MAIS SANS GESTION DU HIGHLIGHT(j'ai dû commenter chacun des php individuellement pour eviter qu'ils se lançent)-->
  <!--<?php /*foreach ($pages as $page):*/ ?>
    <li class="nav-item">
      <a class="nav-link" href="pages/<?php /*echo $page['id'];*/?>/<?php /*echo $page['slug'];*/ ?>"><?php /*echo $page['titre'];*/ ?></a>
    </li>
  <?php /*endforeach;*/ ?>-->
