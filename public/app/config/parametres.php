<?php
/*
    ./app/config/parametres.php
    Paramètres de l'application
 */

 // Constantes de connexion
   define('DBHOTE', '127.0.0.1:3306');
   define('DBNAME', 'creatifs');
   define('DBUSER', 'root');
   define('DBPWD' , 'root');
   define('PUBLIC_PATH', 'public');
   define('ADMIN_PATH' , 'backoffice');

 // Zones dynamiques du template
   $content1 = '';
   $title    = '';
   $scripts  = '';

 // Textes
   define('TITRE_ENVOI_FORMULAIRE', 'Envoi du formulaire');
   define('TITRE_LOGIN', 'Connexion au backOffice');
