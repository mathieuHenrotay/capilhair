<?php
/*
    ./app/controleurs/projetsControleur.php
    Contrôleur des projets
*/
namespace App\Controleurs\Projets;
use \App\Modeles\Projet;
function indexAction(\PDO $connexion, ARRAY $params=[]){
  //je demande au modèle de m'envoyer la liste des projets
  include_once '../app/modeles/projetsModele.php';
  $projets=Projet\findAll($connexion, $params);
  //je charge les scripts nécessaire à l'ajax
  GLOBAL $scripts;
  $scripts='<script src="js/posts/ajaxIndex.js"></script>';
  //j'ajoute la vue index;
  include '../app/vues/projets/index.php';
}

function ajaxIndexAction(\PDO $connexion, ARRAY $params=[]){
  //je demande au modèle de m'envoyer la liste des projets
  include_once '../app/modeles/projetsModele.php';
  $projets=Projet\findAll($connexion, $params);
  //j'ajoute la vue liste;
  include '../app/vues/projets/partials/liste.php';
}

function showAction(\PDO $connexion, INT $id=null){
  //je demande au modèle de m'envoyer les détails du projet
  include_once '../app/modeles/projetsModele.php';
  $projet=Projet\findOneById($connexion, $id);
  //je demande au modèle des tags de m'envoyer les tags en rapport avec le projet
  include_once '../app/modeles/tagsModele.php';
  $tags=\App\Modeles\Tag\findAllByProjetId($connexion, [
    'projetId'=>$id,
    'orderBy'=>'nom',
    'orderSens'=>'ASC'
  ]);
  //je charge la vue show dans content1
  GLOBAL $content1, $title;
  $title=$projet['titre'];
  ob_start();
    include '../app/vues/projets/show.php';
  $content1=ob_get_clean();
}
