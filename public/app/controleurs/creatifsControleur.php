<?php
/*
    ./app/controleurs/creatifsControleur.php
    Contrôleur des creatifs
*/
namespace App\Controleurs\Creatifs;
use \App\Modeles\Creatif;
function indexAction(\PDO $connexion, ARRAY $params=[]){
  //je demande au modèle de m'envoyer la liste des creatifs
  include_once '../app/modeles/creatifsModele.php';
  $creatifs=Creatif\findAll($connexion, $params);
  //je charge la vue index
  include '../app/vues/creatifs/index.php';
}

function showAction(\PDO $connexion, INT $id=null){
  //je demande au modèle de m'envoyer les détails du creatif
  include_once '../app/modeles/creatifsModele.php';
  $creatif=Creatif\findOneById($connexion, $id);
  //je demande au modèle des projets de m'envoyer les projets en rapport avec le creatif
  include_once '../app/modeles/projetsModele.php';
  $projets=\App\Modeles\Projet\findAllByCreatifId($connexion, [
    'orderBy'=>'dateCreation',
    'orderSens'=>'DESC',
    'id'=>$id
  ]);
  //je charge la vue show dans content1
  GLOBAL $content1, $title;
  $title=$creatif['pseudo'];
  ob_start();
    include '../app/vues/creatifs/show.php';
  $content1=ob_get_clean();
}
