<?php
/*
    ./app/controleurs/abonnesControleur.php
    Contrôleur des abonnes
*/
namespace App\Controleurs\Abonnes;
use \App\Modeles\Abonne;
function addAction(\PDO $connexion, STRING $mail=null){
  //je demande au modele d'ajouter un element à la DB
  include_once '../app/modeles/abonnesModele.php';
  $id=Abonne\insertOne($connexion, $mail);
  //J'affiche un message dans content1
  GLOBAL $content1, $title;
  $title=TITRE_ENVOI_FORMULAIRE;
  ob_start();
    include '../app/vues/abonnes/add.php';
  $content1=ob_get_clean();
}
