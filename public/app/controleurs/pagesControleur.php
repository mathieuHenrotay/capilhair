<?php
/*
    ./app/controleurs/pagesControleur.php
    Contrôleur des pages
*/
namespace App\Controleurs\Pages;
use \App\Modeles\Page;
function showAction(\PDO $connexion, INT $id=1){
  //je demande au modèle de m'envoyer les détails de la page
  include_once '../app/modeles/pagesModele.php';
  $page=Page\findOneById($connexion, $id);
  //je charge la vue show dans content1
  GLOBAL $content1, $title;
  $title=$page['titre'];
  ob_start();
    include '../app/vues/pages/show.php';
  $content1=ob_get_clean();
}
function indexAction(\PDO $connexion, ARRAY $params=[]){
  //je demande au modèle de m'envoyer la liste des pages
  include_once '../app/modeles/pagesModele.php';
  $pages=Page\findAll($connexion, $params);
  //je charge la vue index
  include '../app/vues/pages/index.php';
}
