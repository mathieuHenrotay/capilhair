<?php
/*
    ./app/controleurs/tagsControleur.php
    Contrôleur des tags
*/
namespace App\Controleurs\Tags;
use \App\Modeles\Tag;
function indexAction(\PDO $connexion, ARRAY $params=[]){
  //je demande au modèle de m'envoyer la liste des tags
  include_once '../app/modeles/tagsModele.php';
  $tags=Tag\findAll($connexion, $params);
  //je charge la vue index directement
  include '../app/vues/tags/index.php';
}
function showAction(\PDO $connexion, INT $id=null){
  //je demande au modèle de m'envoyer les détails du tag
  include_once '../app/modeles/tagsModele.php';
  $tag=Tag\findOneById($connexion, $id);
  //je demande au modèle des projets de m'envoyer la liste des projets qui ont le tag
  include_once '../app/modeles/projetsModele.php';
  $projets=\App\Modeles\Projet\findAllByTagId($connexion,[
    'orderBy'=>'dateCreation',
    'orderSens'=>'DESC',
    'id'=>$id
  ]);
  //je charge la vue show dans content1
  GLOBAL $content1, $title;
  $title=$tag['nom'];
  ob_start();
    include '../app/vues/tags/show.php';
  $content1=ob_get_clean();
}
