<?php
/*
    ./app/routeurs/creatifsRouteur.php
    Routeur des creatifs
*/
include_once '../app/controleurs/creatifsControleur.php';
switch ($_GET['creatifs']) {
  case 'show':
    // ROUTE DE DETAILS D'UN CREATIF
    // PATTERN: creatifs/id/slug
    // CTRL:creatifsControleur
    // ACTION: showAction

    \App\Controleurs\Creatifs\showAction($connexion, $_GET['id']);
    break;
}
