<?php
/*
    ./app/routeurs/projetsRouteur.php
    Routeur des projets
*/
include_once '../app/controleurs/projetsControleur.php';
switch ($_GET['projets']) {
  case 'show':
    // ROUTE DE DETAIL D'UN PROJET
    // PATTERN: projets/id/slug²
    // CTRL:projetsControleur
    // ACTION: showAction

    \App\Controleurs\Projets\showAction($connexion, $_GET['id']);
    break;
  case 'ajax':
    switch ($_GET['action']) {
      case 'index':
        // ROUTE DE L'AFFICHAGE DES AUTRES PROJETS EN AJAX
        // PATTERN: projets/ajax/index
        // CTRL:projetsControleur
        // ACTION: ajaxIndexAction

        \App\Controleurs\Projets\ajaxIndexAction($connexion, [
          'orderBy'=>'dateCreation',
          'orderSens'=>'DESC',
          'limit'=>10,
          'offset'=>intval($_POST['offset'])
        ]);
        break;
    }
    break;
}
