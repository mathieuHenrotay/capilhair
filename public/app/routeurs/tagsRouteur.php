<?php
/*
    ./app/routeurs/tagsRouteur.php
    Routeur des tags
*/
include_once '../app/controleurs/tagsControleur.php';
switch ($_GET['tags']) {
  case 'show':
    // ROUTE DE DETAILS D'UN TAG
    // PATTERN: tags/id/slug
    // CTRL:tagsControleur
    // ACTION: showAction

    \App\Controleurs\Tags\showAction($connexion, $_GET['id']);
    break;
}
