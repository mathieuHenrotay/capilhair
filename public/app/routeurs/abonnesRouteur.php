<?php
/*
    ./app/routeurs/abonnesRouteur.php
    Routeur des abonnes
*/
include_once '../app/controleurs/abonnesControleur.php';
switch ($_GET['abonnes']) {
  case 'newsletter':
    // ROUTE DE L'ENVOI DU FORMULAIRE
    // PATTERN: newsletter
    // CTRL:abonnesControleur
    // ACTION: addAction
    
    \App\Controleurs\Abonnes\addAction($connexion, $_POST['mail']);
    break;
}
