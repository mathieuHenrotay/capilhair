<?php
/*
    ./app/routeurs/pagesRouteur.php
    Routeur des pages
*/
include_once '../app/controleurs/pagesControleur.php';
switch ($_GET['pages']) {
  case 'show':
    // ROUTE DE DETAIL D'UN POST
    // PATTERN: pages/id/slug
    // CTRL:pagesControleur
    // ACTION: showAction

    \App\Controleurs\Pages\showAction($connexion, $_GET['id']);
  break;
}
